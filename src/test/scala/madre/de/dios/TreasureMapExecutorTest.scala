package madre.de.dios

import madre.de.dios.GeographyModel.Orientation._
import madre.de.dios.GeographyModel._
import madre.de.dios.StateModels._
import madre.de.dios.TreasureModels.Directive._
import madre.de.dios.TreasureModels._
import madre.de.dios.io.ConsoleOutputs
import org.scalatest.FunSuite

object Fixtures {
  val haceNada = TreasureMap(MapLayout(1, 1), List(), List(), List())

  val Julio = "julio"
  val Pablo = "pablo"
  val Lara = "Lara"

  val julioViaje = AdventurerFeed(AdventurerState(orientation = North, collectedTreasures = 0, point = Point(1, 1)),
                    Adventurer(Julio), Seq(Forward, ToTheLeft, ToTheRight, Forward))
  val pabloViaje = AdventurerFeed(AdventurerState(orientation = North, collectedTreasures = 0, point = Point(1, 1)),
                    Adventurer(Pablo), Seq(Forward, ToTheLeft, ToTheLeft))

  val pabloYJulio = TreasureMap(MapLayout(1, 1), List(), List(), List(julioViaje, pabloViaje))

  val nominalProgram = TreasureMap(
    map = MapLayout(3, 4), // C​ - 3 - 4
    mountains = List(
      Mountain(Point(1, 0)), // M​ - 1 - 0
      Mountain(Point(2, 1))), // M​ - 2 - 1
    treasures = List(
      (TreasureState(currentTreasures = 2), Treasure(Point(0, 3))), // T​ - 0 - 3 - 2
      (TreasureState(currentTreasures = 3), Treasure(Point(1, 3)))), // T​ - 1 - 3 - 3
    adventurers = List(AdventurerFeed(
      AdventurerState(orientation = South, collectedTreasures = 0, point = Point(1, 1)),
      Adventurer(Lara),
      Seq(Forward, Forward, ToTheRight, Forward,
          ToTheRight, Forward, ToTheLeft, ToTheLeft, Forward)))) // A​ - Lara - 1 - 1 - S - AADADAGGA

  val simpleProgram = TreasureMap(
    map = MapLayout(4, 4),
    mountains = List(
      Mountain(Point(1, 0)),
      Mountain(Point(2, 1))),
    treasures = List(
      (TreasureState(currentTreasures = 2), Treasure(Point(0, 3))),
      (TreasureState(currentTreasures = 3), Treasure(Point(1, 3)))),
    adventurers = List(AdventurerFeed(
      AdventurerState(orientation = South, collectedTreasures = 0, point = Point(1, 1)),
      Adventurer(Lara),
      Seq(Forward, ToTheRight, Forward))))
}

class TreasureMapExecutorTest extends FunSuite {

  import Fixtures._

  test("empty program is rightfully executed") {
    assert(TreasureMapExecutor.execute(haceNada)(ConsoleOutputs.squareLike) == MapState.firstState(haceNada))
  }

  test("execute non-empty program without errors") {
    val finalState: MapState = TreasureMapExecutor.execute(pabloYJulio)(ConsoleOutputs.squareLike)

    assert(finalState.adventurerAt(Point(1, 0)).exists(_.name == Julio))
    assert(finalState.adventurerAt(Point(1, 1)).exists(_.name == Pablo))
  }

  test("execute nominal program") {
    val finalState: MapState = TreasureMapExecutor.execute(nominalProgram)(ConsoleOutputs.squareLike)

    assert(finalState.adventurerAt(Point(0, 3)).exists(_.name == Lara))

    assert(finalState.byAdventurerId(Lara)
      .exists { case AdventurerData(_, state) =>
        state.point == Point(0, 3) &&
          state.collectedTreasures == 3
      })

    assert(finalState.treasuresStates.get(Point(0, 3))
      .exists(_.currentTreasures == 0))
  }

  test("execute simple program") {
    val finalState: MapState = TreasureMapExecutor.execute(simpleProgram)(ConsoleOutputs.squareLike)

    assert(finalState.adventurerAt(Point(0, 2)).exists(_.name == Lara))
    assert(finalState.byAdventurerId(Lara).exists { case AdventurerData(_, state) =>
      state.point == Point(0, 2) &&
        state.collectedTreasures == 0
    })
  }
}

class TreasureMapTests extends FunSuite {

  import Fixtures._

  test("steps are correctly organized") {
    assert(haceNada.steps.isEmpty)

    val firstStep = (0, List(AdventurerDirective(Julio, Forward), AdventurerDirective(Pablo, Forward)))
    val secondStep = (1, List(AdventurerDirective(Julio, ToTheLeft), AdventurerDirective(Pablo, ToTheLeft)))
    val thirdStep = (2, List(AdventurerDirective(Julio, ToTheRight), AdventurerDirective(Pablo, ToTheLeft)))
    val fourthStep = (3, List(AdventurerDirective(Julio, Forward)))

    assert(pabloYJulio.steps == List(firstStep, secondStep, thirdStep, fourthStep))
  }
}
