package madre.de.dios.io

import madre.de.dios.GeographyModel.{MapLayout, Orientation, Point}
import madre.de.dios.StateModels.MapState
import madre.de.dios.TreasureModels
import madre.de.dios.TreasureModels.Directive.{Forward, ToTheLeft, ToTheRight}
import madre.de.dios.TreasureModels.{Adventurer, AdventurerFeed, AdventurerState, Mountain, Treasure, TreasureMap, TreasureState}
import org.scalatest.FunSuite

class OutputTest extends FunSuite {
  test("we are able to output a treasuremap in a square fashion") {
    val map: TreasureModels.TreasureMap = TreasureMap(MapLayout(3, 4),
      mountains = List(Mountain(Point(1, 0)), Mountain(Point(2, 1))),
      treasures = List((TreasureState(2), Treasure(Point(0, 3))), (TreasureState(1), Treasure(Point(1, 3)))),
      adventurers = List(AdventurerFeed(
        state = AdventurerState(Orientation.South, 0, Point(1, 1)),
        adventurer = Adventurer("Lara"),
        directives = Seq(Forward, Forward, ToTheRight, Forward, ToTheRight, Forward, ToTheLeft, ToTheLeft, Forward))))

    println(ConsoleOutputs.squareLike.show(MapState.firstState(map)))
  }

  test("we are able to output a treasuremap in a feed fashion") {
    val map: TreasureModels.TreasureMap = TreasureMap(MapLayout(3, 4),
      mountains = List(Mountain(Point(1, 0)), Mountain(Point(2, 1))),
      treasures = List((TreasureState(2), Treasure(Point(0, 3))), (TreasureState(1), Treasure(Point(1, 3)))),
      adventurers = List(AdventurerFeed(
        state = AdventurerState(Orientation.South, 0, Point(1, 1)),
        adventurer = Adventurer("Lara"),
        directives = Seq(Forward, Forward, ToTheRight, Forward, ToTheRight, Forward, ToTheLeft, ToTheLeft, Forward))))

    println(FileOutputs.feedLike.show(MapState.firstState(map)))
  }
}
