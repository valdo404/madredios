package madre.de.dios.io

import madre.de.dios.GeographyModel.{MapLayout, Orientation, Point}
import madre.de.dios.TreasureModels
import madre.de.dios.TreasureModels.Directive.{Forward, ToTheLeft, ToTheRight}
import madre.de.dios.TreasureModels.{Adventurer, AdventurerFeed, AdventurerState, Mountain, Treasure, TreasureMap, TreasureState}
import org.scalatest.FunSuite

import scala.io.Source

class CsvLikeReadTest extends FunSuite {
  test("are we able to parse a treasuremap from csv file") {
    val map: TreasureModels.TreasureMap = CsvLikeParsing.fromSource(Source.fromResource("example.txt"))

    assert(map == TreasureMap(MapLayout(3, 4),
      mountains = List(Mountain(Point(1, 0)), Mountain(Point(2, 1))),
      treasures = List((TreasureState(2), Treasure(Point(0, 3))), (TreasureState(1), Treasure(Point(1, 3)))),
      adventurers = List(AdventurerFeed(
        state = AdventurerState(Orientation.South, 0, Point(1, 1)),
        adventurer = Adventurer("Lara"),
        directives = Seq(Forward, Forward, ToTheRight, Forward, ToTheRight, Forward, ToTheLeft, ToTheLeft, Forward)))))
  }
}
