package madre.de.dios

import com.typesafe.scalalogging.LazyLogging

import madre.de.dios.GeographyModel.{Point, Orientation}
import madre.de.dios.TreasureModels.Directive._
import madre.de.dios.TreasureModels.{Directive, _}

import scala.collection.mutable

/** Specific model for states: changes along program execution */
object StateModels {

  object MapState {
    /** Initialize the first treasure map state */
    def firstState(originalMap: TreasureMap): MapState = {
      import originalMap._

      MapState(
        originalMap,
        treasures.map { case (state, treasure) => treasure.point -> state }
          .to(collection.mutable.Map),
        adventurers.map { case AdventurerFeed(state, adv, _) => state.point -> adv }
          .to(collection.mutable.Map),
        adventurers.map { case AdventurerFeed(state, adv, _) => adv.name -> state }
          .to(collection.mutable.Map))
    }
  }

  sealed trait MapEvent
  sealed trait MapError extends MapEvent

  case class OutsideMap(adventurerId: AdventurerId, coords: Point) extends MapError
  case class MountainHit(adventurerId: AdventurerId, coords: Point) extends MapError
  case class OtherAdventurerFound(adventurerId: AdventurerId, coords: Point) extends MapError
  case class TreasureDepleted(adventurerId: AdventurerId, coords: Point) extends MapError

  case class TreasureFound(adventurerId: AdventurerId, coords: Point, orientation: Orientation, remaining: Int, gained: Int) extends MapEvent
  case class ForwardingTo(adventurerId: AdventurerId, coords: Point, orientation: Orientation) extends MapEvent
  case class RotatingTo(adventurerId: AdventurerId, orientation: Orientation, coords: Point) extends MapEvent
  case object NoOp extends MapEvent

  /**
   * Tracks changing stuff along program execution
   * Can be seen as a sink of the directive stream
   *
   * @todo action system can possibly be migrated to a semigroup
   * @todo some parts can be possibly be migrated to a repository
   */
  case class MapState(original: TreasureMap,
                      treasuresStates: mutable.Map[Point, TreasureState],
                      adventurersByPosition: mutable.Map[Point, Adventurer],
                      adventurerStates: mutable.Map[AdventurerId, AdventurerState]) extends LazyLogging {

    def adventurerAt(point: Point): Option[Adventurer] = {
      adventurersByPosition.get(point)
    }

    def treasureAt(point: Point): Option[TreasureData] = {
      for {
        s <- treasuresStates.get(point)
        e <- original.treasureAt(point)
      } yield TreasureData(e, s)
    }

    def treasures = {
      treasuresStates.toList.map(t => TreasureData(Treasure(t._1), t._2))
    }

    def adventurers = {
      adventurerStates.toList.map(t => AdventurerData(Adventurer(t._1), t._2))
    }

    def byAdventurerId(advertiserId: AdventurerId): Option[AdventurerData] = {
      for {
        entity <- original.advById.get(advertiserId)
        state <- adventurerStates.get(advertiserId)
      } yield AdventurerData(entity, state)
    }

    def actOnMap(adventurerDirective: AdventurerDirective): (MapEvent, MapState) = {
      import adventurerDirective._

      byAdventurerId(adventurerId)
        .map(data => {
        if (directive == Forward) {
          val newCoordinates = data.state.point.forward(data.state.orientation)

          if (!original.isInside(newCoordinates)) {
            (OutsideMap(data.entity.id, newCoordinates), this)
          } else {
            actOnForward(data, newCoordinates)
          }
        }
        else {
          rotateAdventurer(data, directive)
        }
      }).getOrElse((NoOp, this))
    }

    /** What happens if we move to the following coordinate ? */
    private def actOnForward(adventurerData: AdventurerData, newPoint: Point): (MapEvent, MapState) = {
      (original.mountainAt(newPoint)
        .map(_ => {
          (MountainHit(adventurerData.entity.id, newPoint), this)
        }))
        .orElse(original.treasureAt(newPoint)
          .map(treasure => {
            openTreasure(adventurerData, treasure)
          }))
        .orElse(adventurerAt(newPoint)
          .map(_ => {
            (OtherAdventurerFound(adventurerData.entity.id, newPoint), this)
          }))
        .getOrElse({
          moveAdventurer(adventurerData, newPoint)
        })
    }

    /**
     * Open a treasure to see what is left inside
     */
    private def openTreasure(adventurer: AdventurerData, treasure: Treasure): (MapEvent, MapState) = {
      treasuresStates.get(treasure.point)
        .map(treasureState => {
          moveAdventurer(adventurer, treasure.point)

          if(treasureState.currentTreasures == 0)
            (TreasureDepleted(adventurer.entity.id, treasure.point), this)
          else {
            giveTreasure(AdventurerData(adventurer.entity, adventurer.state.copy(point = treasure.point)), TreasureData(treasure, treasureState))
          }
        }).getOrElse(
          moveAdventurer(adventurer, treasure.point)
        )
    }

    private def giveTreasure(adventurer: AdventurerData, treasure: TreasureData): (MapEvent, MapState) = {
      val newTreasureState = treasure.state.copy(currentTreasures = treasure.state.currentTreasures - 1)
      val newState = adventurer.state.copy(collectedTreasures = adventurer.state.collectedTreasures + 1)

      adventurerStates.update(adventurer.entity.id, newState)
      treasuresStates.update(treasure.entity.point, newTreasureState)

      (TreasureFound(adventurerId = adventurer.entity.id,
        coords = treasure.entity.point,
        orientation = adventurer.state.orientation,
        remaining = newTreasureState.currentTreasures,
        gained = 1), this)
    }

    /** Move adventurer and update internal states */
    private def moveAdventurer(adventurerData: AdventurerData, to: Point): (MapEvent, MapState) = {
      val newState = adventurerData.state.copy(point = to)

      updateInternal(adventurerData, newState)

      (ForwardingTo(adventurerData.entity.id, to, newState.orientation), this)
    }

    /** Rotate adventurer and update internal states */
    private def rotateAdventurer(adventurerData: AdventurerData, towards: Directive): (MapEvent, MapState) = {
      val orientation = adventurerData.state.orientation.rotate(towards)

      updateInternal(adventurerData, adventurerData.state.copy(orientation = orientation))

      (RotatingTo(adventurerData.entity.id, orientation, adventurerData.state.point), this)
    }

    private def updateInternal(adventurerData: AdventurerData, newState: AdventurerState) = {
      adventurersByPosition.remove(adventurerData.state.point)
      adventurersByPosition.put(newState.point, adventurerData.entity)
      adventurerStates.update(adventurerData.entity.id, newState)
    }
  }

}
