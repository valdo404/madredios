package madre.de.dios

import enumeratum.{Enum, EnumEntry}
import madre.de.dios.TreasureModels.{Directive, Parsed}

object GeographyModel {
  case class MapLayout(height: Int, width: Int) extends Parsed
  case class Point(x: Int, y: Int) {
    def forward(orientation: Orientation): Point = {
      import Orientation._

      orientation match {
        case North => Point(x, y - 1)
        case East => Point(x + 1, y)
        case South => Point(x, y + 1)
        case West => Point(x - 1, y)
      }
    }
  }

  sealed abstract class Orientation(override val entryName: String) extends EnumEntry {
    def rotate(directive: Directive): Orientation = {
      import Directive._

      directive match {
        case ToTheLeft => anti(this)
        case ToTheRight => normal(this)
        case Forward => this
      }
    }
  }

  object Orientation extends Enum[Orientation] {
    val values = findValues

    case object North extends Orientation("N")
    case object East extends Orientation("E")
    case object South extends Orientation("S")
    case object West extends Orientation("O")
  }

  val anti: Map[Orientation, Orientation] = Map(
    (Orientation.North, Orientation.West),
    (Orientation.West, Orientation.South),
    (Orientation.South, Orientation.East),
    (Orientation.East, Orientation.North)
  )

  val normal = anti.map(_.swap)
}
