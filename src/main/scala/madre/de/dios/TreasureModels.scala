package madre.de.dios

import madre.de.dios.GeographyModel.{MapLayout, Orientation, Point}
import madre.de.dios.TreasureModels.{Adventurer, AdventurerState}

/** Stable model for things that do not change along program execution  */
object TreasureModels {
  import enumeratum._

  trait Parsed

  sealed trait Item {
    val point: Point
  }

  case class Mountain(point: Point) extends Item with Parsed
  case class Treasure(point: Point) extends Item

  type AdventurerId = String

  sealed abstract class Directive(override val entryName: String) extends EnumEntry

  object Directive extends Enum[Directive] {
    val values = findValues

    case object Forward extends Directive("A")
    case object ToTheLeft extends Directive("G")
    case object ToTheRight extends Directive("D")
  }

  case class Adventurer(name: AdventurerId) {
    val id: AdventurerId = name
  }

  case class AdventurerDirective(adventurerId: AdventurerId, directive: Directive)
  case class AdventurerState(orientation: Orientation, collectedTreasures: Int, point: Point) extends Item with Parsed
  case class TreasureState(currentTreasures: Int) extends Parsed
  case class AdventurerData(entity: Adventurer, state: AdventurerState)
  case class TreasureData(entity: Treasure, state: TreasureState) extends Parsed
  case class AdventurerFeed(state: AdventurerState, adventurer: Adventurer, directives: Seq[Directive]) extends Parsed

  case class TreasureMap(map: MapLayout,
                         mountains: List[Mountain],
                         treasures: List[(TreasureState, Treasure)],
                         adventurers: List[AdventurerFeed]
                        ) {
    val treasureByPosition = treasures
      .map(_._2)
      .map(tr => tr.point -> tr)
      .to(collection.mutable.Map)

    val mountainsByPosition = mountains.map(tr => tr.point -> tr)
      .to(collection.mutable.Map)

    val advById = adventurers.map(_.adventurer).map(adv => adv.name -> adv).toMap

    def mountainAt(point: Point): Option[Mountain] = {
      mountainsByPosition.get(point)
    }

    def treasureAt(point: Point): Option[Treasure] = {
      treasureByPosition.get(point)
    }

    def isInside(point: Point): Boolean = {
      0 <= point.x && point.x <= map.width && 0 <= point.y && point.y <= map.height
    }

    def steps: Seq[(Int, Seq[AdventurerDirective])] = {
      adventurers
        .flatMap { case AdventurerFeed(_, adv, directives) =>
          directives.zipWithIndex
            .map(_.swap)
            .map {
              case (step, directive) => (step, adv.name, directive)
            }
        }
        .groupMap { case (step, _, _) => step } { case (_, id, directive) => AdventurerDirective(id, directive) }
      }.toSeq.sortBy(_._1)
  }

}