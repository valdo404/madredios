package madre.de.dios

import madre.de.dios.io.{ConsoleOutputs, CsvLikeParsing, FileOutputs}

import scala.io.Source

object OhMadreMio extends App {
  val treasureMap = CsvLikeParsing.fromSource(Source.stdin)

  val state = TreasureMapExecutor.execute(treasureMap)(ConsoleOutputs.squareLike)

  Console.out.print(FileOutputs.feedLike.show(state))
}
