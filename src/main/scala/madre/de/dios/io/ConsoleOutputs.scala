package madre.de.dios.io

import cats.Show
import cats.syntax.show._
import madre.de.dios.GeographyModel.Point
import madre.de.dios.StateModels.MapState
import madre.de.dios.TreasureModels.{Adventurer, Mountain, TreasureData, TreasureState}

object ConsoleOutputs {
  import cats.implicits._

  private implicit val adventurerShow = Show.show[Adventurer](adv => {
    s"A[${adv.id.take(4)}]"
  })

  private implicit val mountainShow = Show.show[Mountain](_ => {
    s"M"
  })

  private implicit val treasureShow = Show.show[TreasureData](data => {
    s"T[${data.state.currentTreasures}]"
  })


  val squareLike = Show.show[MapState](state => {
    (0 until state.original.map.width).map(
      column => {
        ((0 until state.original.map.height)
          .map(row => renderPoint(state, Point(row, column)))).mkString(" ")
      }
    ).mkString("\n")
  })

  private def renderPoint(state: MapState, point: Point): String= {
    ((state.adventurerAt(point).map(_.show))
      .orElse(state.original.mountainAt(point).map(_.show))
      .orElse(state.treasureAt(point).map(_.show))
      .getOrElse(".")).padTo(8, ' ')
  }
}
