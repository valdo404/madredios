package madre.de.dios.io

import enumeratum.{Enum, EnumEntry}
import madre.de.dios.GeographyModel.{MapLayout, Orientation, Point}
import madre.de.dios.TreasureModels._

import scala.io.Source

trait TreasureMapParsing {
  def fromSource(source: Source): TreasureMap
}

object CsvLikeParsing extends TreasureMapParsing {
  sealed abstract class CsvCase(override val entryName: String) extends
    EnumEntry

  object CsvCase extends Enum[CsvCase] {
    val values = findValues

    case object Map extends CsvCase("C")
    case object Mountain extends CsvCase("M")
    case object Treasure extends CsvCase("T")
    case object Adventurer extends CsvCase("A")
  }

  override def fromSource(source: Source): TreasureMap = {
    parsedToMap(source.getLines().map(parseLine).toList
      .groupMap(_._1)(_._2))
  }

  private def parsedToMap(value: Map[CsvCase, List[Parsed]]): TreasureMap = {
    TreasureMap(
      map = value(CsvCase.Map).head.asInstanceOf[MapLayout],
      mountains = value(CsvCase.Mountain).map(_.asInstanceOf[Mountain]),
      treasures = value(CsvCase.Treasure).map(_.asInstanceOf[TreasureData]).map(data => data.state -> data.entity),
      adventurers = value(CsvCase.Adventurer).map(_.asInstanceOf[AdventurerFeed]))
  }

  private def parseLine(line: String): (CsvCase, Parsed) = {
    CsvCase.withNameInsensitive(line.take(1)) match {
      case c @ CsvCase.Map  => (c, parseMap(line))
      case c @ CsvCase.Mountain  => (c, parseMountain(line))
      case c @ CsvCase.Treasure  => (c, parseTreasure(line))
      case c @ CsvCase.Adventurer  => (c, parseAdventurer(line))
    }
  }

  // C​ - 3 - 4
  def parseMap(line: String): MapLayout = {
    val List(_, height, width) = line.split("-").map(_.trim).toList

    MapLayout(height.toInt, width.toInt)
  }

  // M​ - 2 - 1
  def parseMountain(line: String): Mountain = {
    val List(_, x, y) = line.split("-").map(_.trim).toList

    Mountain(Point(x.toInt, y.toInt))
  }

  // T​ - 0 - 3 - 1
  def parseTreasure(line: String): TreasureData = {
    val List(_, x, y, count) = line.split("-").map(_.trim).toList

    TreasureData(Treasure(Point(x.toInt, y.toInt)), TreasureState(count.toInt))
  }

  // A​ - Lara - 1 -1 - S - AADADAGGA
  def parseAdventurer(line: String): AdventurerFeed = {
    val List(_, name, x, y, orientation, directives) = line.split("-").map(_.trim).toList

    AdventurerFeed(
      AdventurerState(
        orientation = Orientation.withNameInsensitive(orientation), collectedTreasures = 0,
        point = Point(x.toInt, y.toInt)),
      Adventurer(name = name),
      directives.map(c => Directive.withNameInsensitive(c.toString))
    )
  }
}