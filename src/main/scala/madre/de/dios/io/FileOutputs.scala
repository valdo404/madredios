package madre.de.dios.io

import cats.Show
import madre.de.dios.GeographyModel.{MapLayout, Point}
import madre.de.dios.StateModels.MapState
import madre.de.dios.TreasureModels.{Adventurer, AdventurerData, Mountain, TreasureData, TreasureState}

object FileOutputs {
  import cats.implicits._

  private implicit val layoutShow = Show.show[MapLayout](layout => {
    s"C - ${layout.height} - ${layout.width}"
  })

  private implicit val mountainShow = Show.show[Mountain](mountain => {
    s"M - ${mountain.point.x} - ${mountain.point.y}"
  })

  private implicit val adventurerShow = Show.show[AdventurerData](adv => {
    s"A - ${adv.entity.id} - ${adv.state.point.x} - ${adv.state.point.y} - ${adv.state.orientation} - ${adv.state.collectedTreasures}"
  })

  private implicit val treasureShow = Show.show[TreasureData](tr => {
    s"T - ${tr.entity.point.x} - ${tr.entity.point.y} - ${tr.state.currentTreasures}"
  })

  val feedLike = Show.show[MapState](state => {
    state.original.map.show + "\n" +
      state.original.mountains.map(_.show).mkString("", "\n", "\n") +
      state.treasures.map(_.show).mkString("", "\n", "\n") +
      state.adventurers.map(_.show).mkString("", "\n", "\n")
  })
}
