package madre.de.dios

import cats.Show
import com.typesafe.scalalogging.LazyLogging
import madre.de.dios.StateModels.MapState
import madre.de.dios.TreasureModels.{AdventurerDirective, TreasureMap}

object TreasureMapExecutor extends LazyLogging {
  def execute(treasureMap: TreasureMap)
             (implicit show: Show[MapState]): MapState = treasureMap.steps.foldLeft(MapState.firstState(treasureMap))(
    (state, step) => {
        executeStep(state, step._2)
    })

  private def executeStep(state: MapState, step: Seq[AdventurerDirective])
                         (implicit show: Show[MapState]): MapState = {
    step.foldLeft(state){ case (mapState, adventurerDirective) =>
      val newState = mapState.actOnMap(adventurerDirective)._2
      Console.err.println("-".padTo(20, '-'))
      Console.err.println(show.show(newState))
      newState}
  }
}
