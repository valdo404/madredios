organization := "pe.gov.turismo"
maintainer := "mapadetesoros@turismo.gov.pe"
name := "treasuremap"

version := "0.1"

scalaVersion := "2.13.1"

libraryDependencies += "com.beachape" %% "enumeratum" % "1.5.13"
libraryDependencies += "org.typelevel" %% "cats-core" % "2.0.0"

libraryDependencies += "org.scalactic" %% "scalactic" % "3.0.8"% "test"
libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.8" % "test"

libraryDependencies += "ch.qos.logback" % "logback-classic" % "1.2.3"
libraryDependencies += "com.typesafe.scala-logging" %% "scala-logging" % "3.9.2"

lazy val root = (project in file("."))
  .enablePlugins(JavaAppPackaging)

